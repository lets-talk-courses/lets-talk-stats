# Linear Mixed Effects models {.transition}

(Or multilevel, hierarchical, etc.)


## We usually have multiple data points of...


- each participant

- the same word

- the same device

- ...

. . .


So far we were assuming [independent and identically distributed]{.emph} variables

. . .

And data points coming from the same person are definitely not independent

## They may not deviate much from the overall trend (in principle)

```{r}
#| echo: false
#| fig-align: center
source("simulated-continuous-data.R")

l.cont.data <- continuous.data %>% 
  pivot_longer(
    s:tx,
    names_to = "phone",
    values_to = "cog"
  )

levels <- l.cont.data %>% 
  group_by(speaker, phone) %>% 
  summarise(
    mean = mean(cog),
    median = median(cog),
    sd = sd(cog),
    min = min(cog),
    max = max(cog),
  ) %>% 
  ungroup() %>% 
  arrange(desc(mean)) %>% distinct(phone) %>% pull(phone)

summar <- l.cont.data %>% 
  group_by(speaker, phone) %>% 
  summarise(mean = mean(cog),
            median = median(cog),
            sd = sd(cog),
            min = min(cog),
            max(cog)) %>% 
  mutate(phone = fct_relevel(phone, levels))

summar %>% 
  filter(speaker %in% sample(unique(l.cont.data$speaker), size = 9)) %>% 
  ggplot(aes(x = phone, y = mean, color = phone)) +
  geom_point(size = 3) +
  geom_linerange(aes(x = phone, ymin = mean - 2 * sd, ymax = mean + 2 * sd, color = phone),
                 linewidth = 1) +
  geom_text(
    inherit.aes = FALSE,
    aes(x = phone, y = mean - 3 * sd, label = phone),
    size = 7
  ) +
  scale_color_manual(values = my.colors[c(1:3,5:7)]) +
  labs(x = NULL,
       y = "CoG (Hz)",
       title = "but still we need to account for them") +
  facet_wrap(~ speaker) +
  theme(
    legend.position = "none",
    panel.grid = element_blank()
  )
```

# Mixed effects {.transition}

## Fixed (or population-level) effects

Variables that correlate with changes in our dependent variable

[Age]{.emph} in F0

Penguin [species]{.emph} in bill depth

[Place of articulation or manner] in [insert an acoustic measure here]

[Hours of sleep]{.emph} in speech errors per minute

[Group]{.emph} (treatment / control) in outcome

## Random (group-level) effects or adjustments

Clusters in variables that deviate from the population-level effect

- Speaker / writer

- Word

- Recorder / device?

. . .

Does [not affect the *real* pattern at the population level]{.emph}


## 

```{r}
#| echo: false
#| fig-align: center
#| label: individual-means-around-the-population

n.speaker <- 40
n.obs.speaker <- 50
tau.speaker <- rnorm(n.speaker, 0, 30)

pop.mean <- 950
pop.sd <- 40

generative.df <- tibble(
  sp = paste0("sp_", 1:n.speaker),
  tau.speaker
)

df <- data.frame(f1 = rnorm(n.obs.speaker, pop.mean + generative.df$tau.speaker[1], pop.sd),
                 speaker = generative.df$sp[1])

for (sp in 2:n.speaker) {
  sp.df <- data.frame(f1 = rnorm(n.obs.speaker, pop.mean + generative.df$tau.speaker[sp], pop.sd),
                 speaker = generative.df$sp[sp])
  
  df <- bind_rows(df, sp.df)
}

hist.dens <- df %>%
  dplyr::filter(speaker %in% sample(unique(df$speaker), 6)) %>% 
  ggplot(aes(x = f1, after_stat(density))) +
  geom_histogram(fill = accent, color = "black") +
  geom_density(aes(x = f1, color = speaker), linewidth = 0.8) +
  theme(legend.position = "none",
        panel.grid = element_blank()) +
  scale_color_manual(values = my.colors) +
  scale_y_continuous(expand = c(0, 0), limits = c(-0.0001, 0.02)) +
  labs(x = "F1", y = "Density")

dist.to.mean <- df %>% 
  ggplot() +
  geom_vline(xintercept = mean(df$f1),
             linewidth = 1) +
  geom_segment(
    inherit.aes = FALSE,
    data = df %>% group_by(speaker) %>% summarise(f1.mean = mean(f1),
                                                  dist.to.mean = f1.mean - mean(df$f1)),
    mapping = aes(x = f1.mean, xend = f1.mean, y = 0, yend = dist.to.mean, color = speaker),
    linewidth = 1
  ) +
  scale_color_grey() +
  scale_y_continuous(expand = c(0,0)) +
  theme(legend.position = "none",
        panel.grid = element_blank()) +
  labs(x = "F1", y = "Distance to mean")


hist.dens + dist.to.mean
```


## We need to account for that variation

Otherwise we are [violating a]{.emph} key [assumption]{.emph} of linear regression:

- [Independent and equally distributed]{.emph} random variables

. . .

We can and should [account for individual differences]{.emph}:

- It gives us a measure of how individuals behave

- Potentially identify different tendencies

- A better understanding of the phenomena

## Two levels of adjustment

<br><br>

- Intercept

- Slope

## Sleep study data

```{r}
#| echo: false
#| fig-align: center

sleepstudy %>% 
  dplyr::filter(Subject %in% sample(unique(sleepstudy$Subject), size = 6)) %>% 
  ggplot(aes(x = Days, y = Reaction)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_point(size = 0.5) +
  scale_x_continuous(n.breaks = 9) +
  labs(x = "Days of sleep deprivation",
       y = "Reaction time (ms)",
       title = "How does sleep deprivation affect reaction times?") +
  facet_wrap(~ Subject) +
  scale_x_continuous(n.breaks = 10)
```

## We transform the reaction times taking the logarithm

```{r}
#| echo: false
#| fig-align: center

sleepstudy <- sleepstudy %>% 
  mutate(
    log.rt = log(Reaction)
  )

histogram.rt <- sleepstudy %>% 
  ggplot(aes(x = Reaction)) +
  geom_histogram(color = "black", fill = accent, alpha = 0.5) +
  labs(x = "Reaction time (ms)",
       y = "Count",
       title = "Raw reaction times are skewed")

histogram.log.rt <- sleepstudy %>% 
  ggplot(aes(x = log.rt)) +
  geom_histogram(color = "black", fill = my.colors[1], alpha = 0.5) +
  labs(x = "Reaction time (log-ms)",
       y = "Count",
       title = "Log-transformed reaction times approach the normal better") +
  scale_x_continuous(n.breaks = 10)

histogram.rt + histogram.log.rt
```



## Intercept adjustment

Let's recall what the intercept is

. . .

$$
\hat{y_i} = \alpha + \beta_1 \times 0 + \beta_2 \times 0 + ... \beta_k \times 0 + \epsilon_i
$$

. . .

### When adjusting the intercept

$$
\hat{y_{i_j}} = \alpha + \tau_j + \beta_1x_1 + \beta_2x_2 + ... \beta_kx_k + \epsilon_i
$$


## Fitting a model with a by-speaker intercept adjustment

:::: .columns

::: {.column width="50%"}

```{r}
fit.sleep <- lm(log.rt ~ Days, data = sleepstudy)

summary(fit.sleep)
```

:::

::: {.column width="50%"}


```{r}

fit.sleep.intercept <- lmer(log.rt ~ Days + (1 | Subject),
     data = sleepstudy)


summary(fit.sleep.intercept)
```

:::

## Understanding the results: population level

```{r}
#| echo: false

results.sleep.intercept <- broom.mixed::tidy(fit.sleep.intercept, conf.int = TRUE, effect = "fixed") %>% select(term, estimate, contains("conf"))

results.sleep.intercept %>% mutate_if(is.numeric, round, 3) %>% gt()
```

## Visualizing the results
```{r}
#| echo: false
#| fig-align: center

days <- 0:9

preds <- tibble(
  days,
  estimated.rt = exp(results.sleep.intercept$estimate[1] + days * results.sleep.intercept$estimate[2]),
  estim.conf.low = exp(results.sleep.intercept$conf.low[1] + days * results.sleep.intercept$conf.low[2]),
  estim.conf.high = exp(results.sleep.intercept$conf.high[1] + days * results.sleep.intercept$conf.high[2])
) 

preds %>% 
  ggplot(aes(x = days, y = estimated.rt)) +
  geom_point(
    data = sleepstudy,
    aes(x = Days, y = Reaction)
  ) +
  geom_line(linewidth = 1, color = accent) +
  geom_ribbon(
    aes(x = days, ymin = estim.conf.low, ymax = estim.conf.high),
    alpha = 0.2, fill = accent
  ) +
  labs(
    x = "Days",
    y = "Reaction times (ms)"
  ) +
  scale_x_continuous(n.breaks = 10)

```

## Visualizing the by-subject intercept adjustments

```{r}
#| echo: false
#| fig-align: center

subject.preds <- ranef(fit.sleep.intercept) %>% 
  as_tibble() %>% 
  select(grp, condval)

subject.preds$subj.intercept <- subject.preds$condval + results.sleep.intercept$estimate[1]

subject.preds <- subject.preds %>%
  slice(rep(1:n(), each = 10))

subject.preds$days <- rep(days, times = n_distinct(subject.preds$grp))

subject.preds <- subject.preds %>% 
  mutate(
    preds = exp(subj.intercept + days * results.sleep.intercept$estimate[2]),
    Subject = grp
  )

all.preds <- preds %>% 
  ggplot(aes(x = days, y = estimated.rt)) +
  geom_point(
    data = sleepstudy,
    aes(x = Days, y = Reaction)
  ) +
  geom_line(linewidth = 1, color = accent) +
  geom_line(
    data = subject.preds,
    mapping = aes(x = days, y = preds, color = Subject)
  ) +
  geom_ribbon(
    aes(x = days, ymin = estim.conf.low, ymax = estim.conf.high),
    alpha = 0.2, fill = accent
  ) +
  labs(
    x = "Days",
    y = "Reaction times (ms)"
  ) +
  theme(legend.position = "none") +
  viridis::scale_color_viridis(discrete = TRUE, option = "H")

faceted.preds <- preds %>% 
  ggplot(aes(x = days, y = estimated.rt)) +
  geom_point(
    data = sleepstudy,
    aes(x = Days, y = Reaction)
  ) +
  geom_line(linewidth = 1, color = accent) +
  geom_line(
    data = subject.preds,
    mapping = aes(x = days, y = preds, color = Subject)
  ) +
  geom_ribbon(
    aes(x = days, ymin = estim.conf.low, ymax = estim.conf.high),
    alpha = 0.2, fill = accent
  ) +
  labs(
    x = "Days",
    y = "Reaction times (ms)"
  ) +
  theme(legend.position = "none") +
  viridis::scale_color_viridis(discrete = TRUE, option = "H") +
  facet_wrap(~ Subject) +
  scale_x_continuous(n.breaks = 10)

all.preds

```

## Visualizing the by-subject intercept adjustments

```{r}
#| echo: false
#| fig-align: center

faceted.preds
```


## Intercept and slope adjustment


Let's recall what the slope is

. . .

The change in y for a 1-unit increase in $x_i$

. . .

### When adjusting the intercept and the slope

$$
\hat{y_{i_j}} = \alpha + \tau_j + \beta_1x_1 + \beta_{1_j}x_1 + \beta_2x_2 + ... \beta_kx_k + \epsilon_i
$$


## Fitting a model with a by-speaker intercept and slope adjustment

```{r}
fit.sleep.int.slp <- lmer(log.rt ~ Days + (1 + Days | Subject),
                          data = sleepstudy)

summary(fit.sleep.int.slp)
```

## Understanding the results: population level

```{r}
#| echo: false

results.sleep.slp <- broom.mixed::tidy(fit.sleep.int.slp,
                                       conf.int = TRUE,
                                       effect = "fixed") %>% select(term, estimate, contains("conf"))

results.sleep.slp %>%
  mutate_if(is.numeric, round, 3) %>% gt()
```


## Visualizing the results

```{r}
#| echo: false
#| fig-align: center


preds <- tibble(
  days,
  estimated.rt = exp(results.sleep.slp$estimate[1] + days * results.sleep.slp$estimate[2]),
  estim.conf.low = exp(results.sleep.slp$conf.low[1] + days * results.sleep.slp$conf.low[2]),
  estim.conf.high = exp(results.sleep.slp$conf.high[1] + days * results.sleep.slp$conf.high[2])
) 

preds %>% 
  ggplot(aes(x = days, y = estimated.rt)) +
  geom_point(
    data = sleepstudy,
    aes(x = Days, y = Reaction)
  ) +
  geom_line(linewidth = 1, color = accent) +
  geom_ribbon(
    aes(x = days, ymin = estim.conf.low, ymax = estim.conf.high),
    alpha = 0.2, fill = accent
  ) +
  labs(
    x = "Days",
    y = "Reaction times (ms)"
  ) +
  scale_x_continuous(n.breaks = 10)
```

## Visualizing the by-subject intercept and slope adjustments

```{r}
#| echo: false
#| fig-align: center

subject.preds <- ranef(fit.sleep.int.slp) %>% 
  as_tibble() %>% 
  select(term, grp, condval)

subject.preds <- subject.preds %>% 
  pivot_wider(names_from = "term",
              values_from = "condval") %>% 
  rename(intercept = `(Intercept)`)

subject.preds$subj.intercept <- subject.preds$intercept + results.sleep.intercept$estimate[1]

subject.preds <- subject.preds %>%
  slice(rep(1:n(), each = 10))

subject.preds$days <- rep(days, times = n_distinct(subject.preds$grp))

subject.preds <- subject.preds %>% 
  mutate(
    preds = exp(subj.intercept + days * results.sleep.intercept$estimate[2] + days * Days),
    Subject = grp
  )

all.preds.slp <- preds %>% 
  ggplot(aes(x = days, y = estimated.rt)) +
  geom_point(
    data = sleepstudy,
    aes(x = Days, y = Reaction)
  ) +
  geom_line(linewidth = 1, color = accent) +
  geom_line(
    data = subject.preds,
    mapping = aes(x = days, y = preds, color = Subject)
  ) +
  geom_ribbon(
    aes(x = days, ymin = estim.conf.low, ymax = estim.conf.high),
    alpha = 0.2, fill = accent
  ) +
  labs(
    x = "Days",
    y = "Reaction times (ms)"
  ) +
  theme(legend.position = "none") +
  viridis::scale_color_viridis(discrete = TRUE, option = "H") +
  scale_x_continuous(n.breaks = 10)

faceted.preds.slp <- preds %>% 
  ggplot(aes(x = days, y = estimated.rt)) +
  geom_point(
    data = sleepstudy,
    aes(x = Days, y = Reaction)
  ) +
  geom_line(linewidth = 1, color = accent) +
  geom_line(
    data = subject.preds,
    mapping = aes(x = days, y = preds, color = Subject)
  ) +
  geom_ribbon(
    aes(x = days, ymin = estim.conf.low, ymax = estim.conf.high),
    alpha = 0.2, fill = accent
  ) +
  labs(
    x = "Days",
    y = "Reaction times (ms)"
  ) +
  theme(legend.position = "none") +
  viridis::scale_color_viridis(discrete = TRUE, option = "H") +
  facet_wrap(~ Subject)

all.preds.slp
```

## 

```{r}
#| echo: false
#| fig-align: center

faceted.preds.slp
```

## Intercept-only vs. intercept and slope adjustments

```{r}
#| echo: false
#| fig-align: center
faceted.preds
```

## Intercept-only vs. intercept and slope adjustments

```{r}
#| echo: false
#| fig-align: center
faceted.preds.slp
```


# Recap {.transition}


## Recap {.scrollable}

. . .

[GLM]{.emph}s assume that [observations]{.emph} are [independent]{.emph} of each other

. . .

If this assumption is not met: We have to account for that lack of independence

[Generalized Mixed Effects Models]{.emph}

. . .

[Fixed / population-level]{.emph} effects: systematic effects on the dependent variable

Should be consistent in new experiments

. . .

[Random / group-level effects]{.emph}: Produce deviation in the systematic effects

Sampling [new individuals]{.emph} that belong to the random effect should yield [different individual adjustments]{.emph}

But the [same variation]{.emph} overall

. . .

We can adjust for the intercept

$\hat{y_{i_j}} = \alpha + \tau_j + \beta_1x_1 + \beta_2x_2 + ... \beta_kx_k + \epsilon_i$

. . .

Or the intercept and the slope

$\hat{y_{i_j}} = \alpha + \tau_j + \beta_1x_1 + \beta_{1_j}x_1 + \beta_2x_2 + ... \beta_kx_k + \epsilon_i$

. . .

[Understand your estimates]{.emph}

Understand the [uncertainty around your estimates]{.emph}

[Visualize]{.emph} the estimates and confidence intervals alongside the data
