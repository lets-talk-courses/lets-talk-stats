# Base rate neglect {.transition}

# What's the probability a given person has breast cancer given a positive result in a screening mammography?

## Let's say a mammography...

1.  returns a positive result 98% of the times where there is breast cancer

. . .

i.e. returns a false negative 2% of the time

. . .

2.  returns a positive result when there is no breast cancer 3% of the time

. . .

i.e. returns a false positive 3% of the time

. . .

### What's the probability a given person has breast cancer given a positive result?


```{r}
#| echo: false
library(magrittr)
library(gt)
test.results <- data.frame(
  `Test result` = c("Positive", "Negative"),
  Yes = c(98, 2),
  No = c(3, 97)
)

test.results %>% 
  gt(
    rowname_col = "Test.result"
  ) %>% 
  tab_header(
    title = "Probabilities of test results"
  ) %>% 
  tab_spanner(
    label = "Has cancer",
    columns = c(Yes, No)
  ) %>% 
  tab_stubhead(label = "Test result") %>% 
  gtExtras::gt_theme_538()

```


## What probability does test sensitivity inform us about?

P(result \| disease)

::: notes
p(negative) = 1-p(positive)
:::

P(result \| no-disease)

. . .

[But it is P(cancer | result) that we want to know!]{.r-fit-text}



## $P(breast\ cancer\ |\ positive\ mammography) =$

$$\frac{P(positive\ |\ cancer) \times P(cancer)}{P(positive\ |\ cancer) \times P(cancer) + P(positive\ |\ no\ cancer) \times P(no\ cancer)} =
$$

```{r}
#| echo: false

p.positive.cancer <- 0.98
p.cancer <- 0.01
p.positive.no.cancer <- 0.03
p.no.cancer <- 0.99

p.cancer.positive <- (p.positive.cancer * p.cancer) / ((p.positive.cancer * p.cancer) + (p.positive.no.cancer * p.no.cancer))
```



P(cancer) = 1% = 0.01

P(no cancer) = 99% = 0.99

$$
= \frac{0.98 \times 0.01}{0.99 \times 0.01 + 0.03 \times 0.99} =
$$

$$
= `r p.cancer.positive`
$$

### Test results don't inform us about the probability that we wanted


## Base rate is important
![](assets/figs/covid-vaccin-no-vaccin.jpg){.r-stretch fig-align="center"}
